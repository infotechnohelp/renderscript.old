<?php

namespace Infotechnohelp\RenderScript\Test\TestCase\Lib;

use Infotechnohelp\RenderScript\Config\Custom\CustomConfig;
use Infotechnohelp\RenderScript\Exception\OutputContainsNeedleException;
use Infotechnohelp\RenderScript\Lib\Unit;
use PHPUnit\Framework\TestCase;

/**
 * Class Unit
 * @package Infotechnohelp\RenderScript\Test\TestCase\Lib
 */
class UnitTest extends TestCase
{

    public function testDefaultNeedle()
    {
        $this->assertEquals("#", (new Unit())->getNeedle());
    }

    public function testCustomNeedle()
    {
        $CustomConfig = (new CustomConfig())->setNeedle("<>");

        $this->assertEquals("<>", (new Unit(null, $CustomConfig))->getNeedle());
    }

    public function testMiddleStageRender()
    {
        $this->assertEquals("#", (new Unit())->render());
    }

    public function testFinalStageRender()
    {
        $this->expectException(OutputContainsNeedleException::class);

        (new Unit())->render(true);
    }

    public function testDeleteNeedle()
    {
        $this->assertEquals("", (new Unit())->deleteNeedle()->render());
    }
}
