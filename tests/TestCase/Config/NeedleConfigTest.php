<?php

namespace Infotechnohelp\RenderScript\Test\TestCase\Config;

use Infotechnohelp\RenderScript\Config\_Static\NeedleConfig;
use PHPUnit\Framework\TestCase;


/**
 * Class NeedleConfigTest
 * @package Infotechnohelp\RenderScript\Test\TestCase\Config
 */
class NeedleConfigTest extends TestCase
{

    public function testGetDefaultNeedleStatically()
    {
        $this->assertEquals("#", NeedleConfig::getDefaultNeedle(NeedleConfig::PHP_CONTEXT));
    }
    
}
