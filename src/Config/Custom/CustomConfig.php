<?php

namespace Infotechnohelp\RenderScript\Config\Custom;

/**
 * Class CustomConfig
 * @package Infotechnohelp\RenderScript\Config
 */
class CustomConfig
{

    private $needle;

    /**
     * @var ReservedNeedleCustomConfig
     */
    private $ReservedNeedleConfig;

    /**
     * @param ReservedNeedleCustomConfig $ReservedNeedleConfig
     * @return CustomConfig
     */
    public function setReservedNeedle(ReservedNeedleCustomConfig $ReservedNeedleConfig): self
    {
        $this->ReservedNeedleConfig = $ReservedNeedleConfig;

        return $this;
    }

    /**
     * @return ReservedNeedleCustomConfig
     */
    public function getReservedNeedle(): ReservedNeedleCustomConfig
    {
        return $this->ReservedNeedleConfig;
    }

    /**
     * @param string $needle
     * @return CustomConfig
     */
    public function setNeedle(string $needle): self
    {
        $this->needle = $needle;
        
        return $this;
    }

    /**
     * @return string
     */
    public function getNeedle(): string
    {
        return $this->needle;
    }

}
