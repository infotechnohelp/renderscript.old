<?php

namespace Infotechnohelp\RenderScript\Config\Custom;

/**
 * Class ReservedNeedleCustomConfig
 * @package Infotechnohelp\RenderScript\Config\Custom
 */
class ReservedNeedleCustomConfig
{
    /**
     * @var null
     */
    private $openingSymbols;

    /**
     * @var null
     */
    private $closingSymbols;

    /**
     * ReservedNeedleCustomConfig constructor.
     * @param string $openingSymbols
     * @param string $closingSymbols
     */
    public function __construct(string $openingSymbols, string $closingSymbols)
    {
        $this->openingSymbols = $openingSymbols;

        $this->closingSymbols = $closingSymbols;
    }

    /**
     * @return null|string
     */
    public function getOpeningSymbols(): string
    {
        return $this->openingSymbols;
    }

    /**
     * @return null|string
     */
    public function getCloningSymbols(): string
    {
        return $this->closingSymbols;
    }

}
