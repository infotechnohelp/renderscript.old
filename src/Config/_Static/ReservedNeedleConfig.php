<?php

namespace Infotechnohelp\RenderScript\Config\_Static;

/**
 * Class ReservedNeedleConfig
 * @package Infotechnohelp\RenderScript\Config\_Static
 */
final class ReservedNeedleConfig
{
    const openingSymbols = "{{{";

    const closingSymbols = "}}}";
}
