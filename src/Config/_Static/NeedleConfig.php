<?php

namespace Infotechnohelp\RenderScript\Config\_Static;

/**
 * Class NeedleConfig
 * @package Infotechnohelp\RenderScript\Config\_Static
 */
final class NeedleConfig
{
   
    const PHP_CONTEXT = 0;

    /*const JS_CONTEXT = 1;
    const HTML_CONTEXT = 2;
    const CSS_CONTEXT = 3;*/

    /**
     * @return array
     */
    private static function needles()
    {
        return ["#", /*"#", "#", "#",*/];
    }

    /**
     * @param int $contextId
     * @return mixed
     */
    public static function getDefaultNeedle(int $contextId)
    {
        return self::needles()[$contextId];
    }

}
