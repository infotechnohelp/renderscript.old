<?php

namespace Infotechnohelp\RenderScript\Lib;

use Infotechnohelp\RenderScript\Config\_Static\NeedleConfig;
use Infotechnohelp\RenderScript\Config\Custom\CustomConfig;
use Infotechnohelp\RenderScript\Exception\OutputContainsNeedleException;

/**
 * Class Unit
 * @package Infotechnohelp\RenderScript\Lib
 */
class Unit
{
    /**
     * @var string
     */
    private $needle;

    /**
     * @var string
     */
    private $output;


    /**
     * Unit constructor.
     * @param string|null $tag
     * @param int $context
     * @param CustomConfig|null $CustomConfig
     */
    public function __construct(
        string $tag = null,
        CustomConfig $CustomConfig = null,
        int $context = NeedleConfig::PHP_CONTEXT
    ) {
        $this->needle = NeedleConfig::getDefaultNeedle($context);

        if ($CustomConfig !== null && $CustomConfig->getNeedle() !== null) {
            $this->needle = $CustomConfig->getNeedle();
        }

        $this->output = $this->needle;
    }

    /**
     * @return string
     */
    public function getNeedle(): string
    {
        return $this->needle;
    }

    /**
     * @return Unit
     */
    public function deleteNeedle(): self
    {
        $this->output = str_replace($this->needle, "", $this->output);

        return $this;
    }

    /**
     * @param bool $final
     * @return string
     * @throws OutputContainsNeedleException
     *
     * Final render must contain no needle (Neither main nor reserved)
     */
    public function render(bool $final = false): string
    {
        if ($final && $this->outputContainsNeedle()) {
            throw new OutputContainsNeedleException;
        }

        return $this->output;
    }

    /**
     * @return bool
     */
    private function outputContainsNeedle(): bool
    {
        if (strpos($this->needle, $this->output) === false) {
            return false;
        }

        return true;
    }

}
