<?php

namespace Infotechnohelp\RenderScript\Exception;

/**
 * Class OutputContainsNeedleException
 * @package Infotechnohelp\RenderScript\Exception
 */
class OutputContainsNeedleException extends \Exception
{

}