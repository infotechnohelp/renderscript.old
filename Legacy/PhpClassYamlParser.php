<?php /** @noinspection PhpUnused */

require __DIR__ . '/vendor/autoload.php'; // @todo Remove this line later

use Symfony\Component\Yaml\Yaml;

class PhpClassYamlParser
{
    /**
     * @var
     */
    private $preparedConfig;

    public function parse(string $ymlString = null): array
    {
        if(empty($ymlString)){
            return [];
        }

        $parsed = (new Yaml())::parse($ymlString);

        $this->preparedConfig = $this->prepare($parsed);

        return $this->preparedConfig;
    }

    private function prepareClassParamConfig(string $title, string $paramType = null, array $paramConfig = null): PhpClassParamTemplateInput
    {
        return (new PhpClassParamTemplateInput())
            ->setTitle($title)
            ->setType($paramType)
            ->setAccess(isset($paramConfig['access']) ? $paramConfig['access'] : null)
            ->setDefaultValue(isset($paramConfig['defaultValue']) ? $paramConfig['defaultValue'] : null)
            ->setSetterReturnType(isset($paramConfig['setterReturnType']) ? $paramConfig['setterReturnType'] : null);
    }

    private function prepare(array $parsed): array
    {
        $result = [];

        foreach ($parsed as $classTitleKey => $config) {

            if ($classTitleKey === '_phpFile') {
                continue;
            }

            list($classTitle, $classType) = array_pad(explode(":", $classTitleKey), 2, null);

            $ClassConfig = (new PhpClassTemplateBunchInput());

            $extends = isset($config['extends']) ? $config['extends'] : null;

            $ClassConfig->setClassInput(
                (new PhpClassTemplateInput())
                    ->setClassTitle($classTitle)
                    ->setClassType($classType)
                    ->setExtends($extends)
            );

            if (isset($config['params'])) {

                foreach ($config['params'] as $paramKey => $paramConfig) {

                    list($paramTitle, $paramType) = array_pad(explode(":", $paramKey), 2, null);

                    $ClassConfig->addNewParamInput($paramTitle, $this->prepareClassParamConfig($paramTitle, $paramType, $paramConfig));
                }
            }

            $result[] = $ClassConfig;

        }

        return $result;
    }
}