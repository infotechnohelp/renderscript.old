<?php /** @noinspection PhpUnused */

class PhpClassTemplateBunchInput extends RenderScriptUnitInput
{
    /**
     * @var PhpClassTemplateInput
     */
    private $classInput;

    /**
     * @var AssocBunch
     */
    private $paramInputs;

    public function __construct()
    {
        $this->paramInputs = new AssocBunch();
    }

    public function setClassInput(PhpClassTemplateInput $PhpClassTemplateInput): self
    {
        $this->classInput = $PhpClassTemplateInput;

        return $this;
    }

    public function getClassInput()
    {
        return $this->classInput;
    }

    public function addNewParamInput(string $title, PhpClassParamTemplateInput $PhpClassParamTemplateInput): self
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $this->paramInputs->set($title, $PhpClassParamTemplateInput);

        return $this;
    }

    public function getParamInputs()
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return $this->paramInputs->all(true);
    }
}
