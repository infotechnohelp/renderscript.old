<?php

class PhpClassTemplate extends RenderScriptTemplate
{
    protected function process(IndexedBunch $processingParams = null)
    {
        /** @var PhpClassConfig $input */
        $input = $this->prepareInput();

        $classType = (!empty($input->getClassType())) ? "{$input->getClassType()} " : null;

        $extends = (!empty($input->getExtends())) ? " extends {$input->getExtends()}" : null;

        return
            "{$classType}class {$input->getClassTitle()}$extends\n" .
            "{\n" .
            "{$this->getNeedle()}\n" .
            "}\n";
    }
}


class PhpFileTemplate extends RenderScriptTemplate
{
    protected function process()
    {
        /** @var PhpFileTemplateInput $input */
        $input = $this->prepareInput();

        /** @var PhpFileTemplateConfig $config */
        $config = $this->prepareConfig();

        $declareStrict = $config->isDeclaredStrict() ? " declare(strict_types = 1);" : null;

        if ($config->hasNamespace() && empty($input->getNamespace())) {
            // @todo Create a separate Exception
            throw new Exception("PhpFileInput does not contain namespace value");
        }

        if (!$config->hasNamespace() && !empty($input) && !empty($input->getNamespace())) {
            // @todo Create a separate Exception
            throw new Exception("PhpFileTemplateConfig namespace value is false, although PhpFileInput has namespace value");
        }

        $namespace = $config->hasNamespace() ? "\nnamespace {$input->getNamespace()};\n" : null;

        $useSection = null;

        if ($config->hasUseSection()) {
            $this->getRoot()->setReservedNeedle('use', $input->getUseSection());

            $useSection = "\n{$this->getReservedNeedleOpening()}use{$this->getReservedNeedleClosing()}";
        }


        $result =
            "<?php$declareStrict\n" .
            "$namespace" .
            "$useSection";

        return $result;
    }

}

class PhpClassParamTemplate extends RenderScriptTemplate
{
    protected function process(int $indentAmount = null)
    {
        /** @var PhpClassParamTemplateInput $input */
        $input = $this->prepareInput();

        $indent = str_repeat("\t", $indentAmount);

        return "\n$indent{$input->getAccess()} \${$input->getTitle()};\n";
    }
}

class PhpClassParamSetterGetterTemplate extends RenderScriptTemplate
{
    protected function process(int $indentAmount = null)
    {
        /** @var PhpClassParamTemplateInput $input */
        $input = $this->prepareInput();

        $ucfirstTitle = ucfirst($input->getTitle());

        $type = (!empty($input->getType())) ? "{$input->getType()} " : null;
        $getterReturnType = (!empty($input->getType())) ? ": {$input->getType()}" : null;

        $defaultValue = RenderScriptHelper::prepareDefaultValueString($input->getDefaultValue());

        $setterReturnType = null;
        $additionalSetterLines = "return \$this;\n";

        switch ($input->getSetterReturnType()) {
            case "self":
                $setterReturnType = ": self";
        }

        $indent = str_repeat("\t", $indentAmount);

        $indent2 = str_repeat("\t", ++$indentAmount);

        // @todo Set tabs automatically
        return "\n{$indent}public function set$ucfirstTitle($type\${$input->getTitle()}$defaultValue)$setterReturnType\n" .
            "$indent{\n" .
            "$indent2\$this->{$input->getTitle()} = \${$input->getTitle()};\n" .
            "$indent2$additionalSetterLines" .
            "$indent}\n" .
            "\n" .
            "{$indent}public function get$ucfirstTitle()$getterReturnType\n" .
            "$indent{\n" .
            "{$indent2}return \$this->{$input->getTitle()};\n" .
            "$indent}\n";
    }
}