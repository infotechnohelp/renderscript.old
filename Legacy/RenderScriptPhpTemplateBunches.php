<?php /** @noinspection PhpUnused */


class PhpClassTemplateBunch extends RenderScriptTemplate
{
    protected function process(int $indentAmount = null)
    {
        /** @var PhpClassTemplateBunchInput $input */
        $input = $this->prepareInput();

        /** @noinspection PhpUnhandledExceptionInspection */
        $this
            ->setInput(new AssocBunch([
                'PhpClassTemplate' => $input->getClassInput(),
                'PhpClassParamTemplate' => null,
                'PhpClassParamSetterGetterTemplate' => null,
            ]))
            ->replaceNeedle(PhpClassTemplate::class);


        $indentAmount++;

        foreach ($input->getParamInputs() as $PhpClassParamTemplateInput) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this
                ->resetInputValue('PhpClassParamTemplate', $PhpClassParamTemplateInput)
                ->prependNeedle(PhpClassParamTemplate::class, $indentAmount);
        }

        foreach ($input->getParamInputs() as $PhpClassParamTemplateInput) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this
                ->resetInputValue('PhpClassParamSetterGetterTemplate', $PhpClassParamTemplateInput)
                ->prependNeedle(PhpClassParamSetterGetterTemplate::class, $indentAmount);
        }


        return $this->getOutput();
    }
}

class PhpClassFileTemplateBunch extends RenderScriptTemplateBunch
{

    protected function process(AssocBunch $processingParams = null)
    {
        if ($processingParams !== null && $processingParams->isset("RenderScriptLabel")) {
            /* @noinspection PhpUnhandledExceptionInspection */
            $this
                ->prependNeedle(StaticTextTemplate::class, $processingParams->get("RenderScriptLabel"))
                ->prependNeedle(EmptyLineTemplate::class, 2);
        }

        /* @noinspection PhpUnhandledExceptionInspection */
        return $this
            // @todo Put indentAmount into config classes, not processing params
            ->prependNeedle(PhpFileTemplate::class, 0 /* indentAmount */)
            ->prependNeedle(EmptyLineTemplate::class, 2 /* line amount*/)
            ->prependNeedle(PhpClassTemplateBunch::class, 0 /* indentAmount */)
            ->renderReservedNeedles()
            ->removeNeedle()
            ->getOutput();
    }

}