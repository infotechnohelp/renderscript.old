<!doctype html>

<title>CodeMirror: YAML mode</title>
<meta charset="utf-8"/>
<link rel=stylesheet href="CodeMirror/doc/docs.css">

<link rel="stylesheet" href="CodeMirror/lib/codemirror.css">
<link rel="stylesheet" href="CodeMirror/theme/ambiance.css">

<script src="CodeMirror/lib/codemirror.js"></script>
<script src="CodeMirror/mode/yaml/yaml.js"></script>
<style>.CodeMirror {
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }</style>
<div id=nav>
    <a href="https://codemirror.net"><h1>CodeMirror</h1></a>

    <ul>
        <li><a href="https://github.com/codemirror/codemirror">Code</a>
    </ul>
    <ul>
        <li><a class=active href="#">YAML</a>
    </ul>
</div>

<style>
    .CodeMirror {
        height: auto;
    }
</style>

<article>
    <h2>YAML mode</h2>
    <form><textarea class="yaml-editor" name="first">
--- # Favorite movies
- Casablanca
- North by Northwest
- The Man Who Wasn't There
--- # Shopping list
[milk, pumpkin pie, eggs, juice]
--- # Indented Blocks, common in YAML data files, use indentation and new lines to separate the key: value pairs
  name: John Smith
  age: 33
--- # Inline Blocks, common in YAML data streams, use commas to separate the key: value pairs between braces
{name: John Smith, age: 33}
---
receipt:     Oz-Ware Purchase Invoice
date:        2007-08-06
customer:
    given:   Dorothy
    family:  Gale

items:
    - part_no:   A4786
      descrip:   Water Bucket (Filled)
      price:     1.47
      quantity:  4

    - part_no:   E1628
      descrip:   High Heeled "Ruby" Slippers
      size:       8
      price:     100.27
      quantity:  1

bill-to:  &id001
    street: |
            123 Tornado Alley
            Suite 16
    city:   East Centerville
    state:  KS

ship-to:  *id001

specialDelivery:  >
    Follow the Yellow Brick
    Road to the Emerald City.
    Pay no attention to the
    man behind the curtain.
...
</textarea></form>


    <textarea class="yaml-editor" name="second">
--- # Favorite movies
- Casablanca
- North by Northwest
- The Man Who Wasn't There
--- # Shopping list
[milk, pumpkin pie, eggs, juice]
--- # Indented Blocks, common in YAML data files, use indentation and new lines to separate the key: value pairs
  name: John Smith
  age: 33
--- # Inline Blocks, common in YAML data streams, use commas to separate the key: value pairs between braces
{name: John Smith, age: 33}
---
receipt:     Oz-Ware Purchase Invoice
date:        2007-08-06
customer:
    given:   Dorothy
    family:  Gale

items:
    - part_no:   A4786
      descrip:   Water Bucket (Filled)
      price:     1.47
      quantity:  4

    - part_no:   E1628
      descrip:   High Heeled "Ruby" Slippers
      size:       8
      price:     100.27
      quantity:  1

bill-to:  &id001
    street: |
            123 Tornado Alley
            Suite 16
    city:   East Centerville
    state:  KS

ship-to:  *id001

specialDelivery:  >
    Follow the Yellow Brick
    Road to the Emerald City.
    Pay no attention to the
    man behind the curtain.
...
</textarea>


    <script>
        //var editor = CodeMirror.fromTextArea(document.getElementById("code"), {});
        //editor.setOption("theme", 'ambiance');
    </script>

    <p><strong>MIME types defined:</strong> <code>text/x-yaml</code>.</p>

</article>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>

    const yamlEditors = [];

    $('.yaml-editor').each(function(index, elem){

        let name = $(elem).attr('name');

        if(yamlEditors[name] !== undefined){
            throw Error('Yaml editor already exists: ' + name);
        }

        yamlEditors[name] = CodeMirror.fromTextArea(elem, {
            lineNumbers: true,
            theme: 'ambiance'
        });
    });

    console.log(yamlEditors.first.getValue());

</script>