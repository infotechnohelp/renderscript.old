<?php

class PhpFileTemplateConfig extends RenderScriptUnitConfig
{

    private $isDeclaredStrict = false;

    private $hasNamespace = false;

    private $hasUseSection = false;

    public function setIsDeclaredStrict(bool $declareStrict = true): self
    {
        $this->isDeclaredStrict = $declareStrict;

        return $this;
    }

    public function isDeclaredStrict()
    {
        return $this->isDeclaredStrict;
    }

    public function setHasNamespace(bool $namespace = true): self
    {
        $this->hasNamespace = $namespace;

        return $this;
    }

    public function hasNamespace()
    {
        return $this->hasNamespace;
    }

    public function setHasUseSection(bool $use = true): self
    {
        $this->hasUseSection = $use;

        return $this;
    }

    public function hasUseSection()
    {
        return $this->hasUseSection;
    }

}