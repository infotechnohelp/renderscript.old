<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require 'Bunch.php';
require 'RenderScript.php';
require 'RenderScriptTemplates.php';
require 'RenderScriptPhpTemplateInputs.php';
require 'RenderScriptPhpTemplateBunchInputs.php';
require 'RenderScriptPhpTemplates.php';
require 'PhpClassYamlParser.php';
require 'RenderScriptPhpTemplateBunches.php';
require 'RenderScriptPhpTemplateConfigs.php';
require 'RenderScriptHelper.php';

$result = [];

$inputYaml = $_POST['yaml'] ??
"ClassTitle:\n" .
" params:\n" .
"  paramTitle:string:";
// @todo Read from file later

//file_get_contents("PhpClass.yml")

foreach ((new PhpClassYamlParser())->parse($inputYaml) as $PhpClassTemplateBunchConfig) {
    /** @noinspection PhpUnhandledExceptionInspection */
    $result[] =
        (new PhpClassFileTemplateBunch())
            ->setConfig(new AssocBunch([
                'PhpFileTemplate' => (new PhpFileTemplateConfig())
                    ->setIsDeclaredStrict()
                //->setHasNamespace()
                //->setHasUseSection(),
            ]))
            ->setInput(new AssocBunch([
                /*'PhpFileTemplate' => (new PhpFileTemplateInput())
                    ->setNamespace("App\\Lib")
                    ->addUseSectionItem("use Infotechnohelp\\AssocBunch;\n")
                    ->addUseSectionItem("use Infotechnohelp\\IndexedBunch;\n"),*/
                'PhpClassTemplateBunch' => $PhpClassTemplateBunchConfig,
            ]))
            ->render(new AssocBunch([
                'RenderScriptLabel' => "/** Generated with RenderScript 0.0.1 More information on https://renderscript.com */"
            ]))
            ->getOutput();
}

?>

<style>
    html, body {
        /*background-color: rgba(230, 230, 255, .25);*/
        background-color: rgba(0, 0, 0, .5);
        color:white;
    }

    textarea {
        width: 100%;

        /*background-color: rgba(255, 255, 255, .75);*/
        background-color: rgba(0, 0, 0, .75);
        color: white;
        border: none;

        padding: 5px;
    }

    .small-text {
        font-size: .75rem;
    }

    input[type="submit"] {
        /*margin: 25px 0 0 0;*/
    }

    h1 > button {
        transform: translateY(-4px);
    }

    pre {
        background-color: rgba(150, 150, 150, .1);
        padding: 5px;
        border: 1px solid grey;
    }

    button, input[type="submit"], input[type="reset"] {
        background: black;
        color: white;
        border: 1px solid black;
        padding: 5px 10px;
        cursor: pointer;
        outline: inherit;
    }

    button:hover, input[type="submit"]:hover, input[type="reset"]:hover{
        border: 1px solid white;
        -webkit-box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.75);
        -moz-box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.75);
        box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.75);
    }

    .large-button {
        font-size: 1.5rem;
        font-weight: bold;
    }
</style>

<?php // $_POST['yaml']?>

<script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js"></script>

<link href="prism (1).css" rel="stylesheet" />
<script src="prism.js"></script>

<pre><code class="language-yaml"><?= file_get_contents('PhpClass.yml') ?></code></pre>

<textarea class="language-yaml"><?= file_get_contents('PhpClass.yml') ?></textarea>



<h1>
    RenderScript <span class="small-text">for devs by devs</span>
    <div class="small-text">renderscript.com</div>
</h1>

<h2>PHP Class file template</h2>

<div>
    *** Add Tab intending in rendering process. Each template has base tab amount and depends on it.
    <br/>
    Add rendering history option
    <br/>
    Add hotkeys for Render action
    <br/>
    Choose between different examples
    <br/>
    Copy to clipboard
    <br/>
    Copy example to actual input button
    <br>
    CodeMirror required
    <br>
    Use some ideas from https://onlineyamltools.com/edit-yaml
</div>

<div>
    <h3>
        Yaml input syntax example
        <button>Copy to input</button>
        <button class="textarea-button">Textarea</button>
    </h3>
    <pre><?= file_get_contents('PhpClass.yml') ?></pre>
</div>

<h2>
    Yaml input
    <div class="small-text">Use space for indenting</div>
</h2>


<form method="POST">
    <textarea name="yaml"
              oninput='this.style.height = "";this.style.height = (this.scrollHeight + 1) +"px"'><?= $inputYaml; ?></textarea>
    <div>Ctrl + Alt + Enter</div>
    <input type="submit" value="Render" class="large-button"/>
</form>


<h1>Result</h1>

<?php $i = 1;
foreach ($result as $script) { ?>
    <div>
        <h3>Script <?= $i ?>
            <button>Copy to clipboard</button>
            <button class="textarea-button">Textarea</button>
        </h3>

        <pre class="prettyprint"><?= htmlspecialchars($script) ?></pre>
    </div>
    <?php $i++;
} ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
    $(() => {
        $(".textarea-button").click(function () {

            let scrollTop = $(window).scrollTop();

            let pre = $(this).parents('div').find('pre');

            let textarea = $('<textarea>' + $(pre).text() + '</textarea>')[0];

            $(pre).replaceWith(textarea);

            textarea.style.height = (textarea.scrollHeight + 1) + "px";

            $(window).scrollTop(scrollTop);
        });

        $('textarea').each(function (indexed, element) {
            element.style.height = (element.scrollHeight + 1) + "px";
        });
    });
</script>
