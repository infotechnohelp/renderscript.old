<?php

class EmptyLineTemplate extends RenderScriptTemplate
{
    protected function process(int $lineAmount)
    {
        return str_repeat("\n", $lineAmount);
    }
}

class StaticTextTemplate extends RenderScriptTemplate
{
    protected function process(string $text = null)
    {
        return $text;
    }
}