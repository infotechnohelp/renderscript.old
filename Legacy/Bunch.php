<?php /** @noinspection PhpUnused */

// EXCEPTIONS

class BunchKeyDoesNotExistException extends Exception
{
    public function __construct(string $inputKey)
    {
        parent::__construct("Bunch key '$inputKey' does not exist");
    }
}

class BunchKeyAlreadyExistsException extends Exception
{
    public function __construct(string $inputKey)
    {
        parent::__construct("Bunch key '$inputKey' already exists");
    }
}

class IncorrectAssocBunchDataException extends Exception
{
    public function __construct(array $data)
    {
        parent::__construct("Provided data array is not associative. JSON representation: " . json_encode($data));
    }
}

class IncorrectIndexedBunchDataException extends Exception
{
    public function __construct(array $data)
    {
        parent::__construct("Provided data array is not sequential (indexed). JSON representation: " . json_encode($data));
    }
}

class EmptyBunchDataException extends Exception
{
    public function __construct()
    {
        parent::__construct("Data is empty");
    }
}

class DataKeyExistsException extends Exception
{
    public function __construct(string $key)
    {
        parent::__construct("Data key '$key' already exists");
    }
}


// CLASSES


abstract class Bunch
{
    /**
     * @var array
     */
    protected $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public abstract function rewriteAll(array $data): self;


    /**
     * @throws EmptyBunchDataException
     */
    protected function countCheck()
    {
        if (count($this->data) === 0) {
            throw new EmptyBunchDataException;
        }
    }


    /**
     * @param bool $empty
     * @return array
     * @throws EmptyBunchDataException
     */
    public function all(bool $empty = false): array
    {
        if (!$empty) {
            $this->countCheck();
        }

        return $this->data;
    }

    public function mergeAppend(array $data): self
    {

        $this->data = array_merge($this->data, $data);

        return $this;
    }

    public function mergePrepend(array $data): self
    {
        $this->data = array_merge($data, $this->data);

        return $this;
    }
}


class IndexedBunch extends Bunch
{
    /**
     * IndexedInput constructor.
     * @param array $data
     * @throws IncorrectIndexedBunchDataException
     */
    public function __construct(array $data = [])
    {
        if (array_values($data) !== $data) {
            throw new IncorrectIndexedBunchDataException($data);
        }

        parent::__construct($data);
    }

    /**
     * @throws EmptyBunchDataException
     */
    public function first()
    {
        $this->countCheck();
        return $this->data[0];
    }

    /**
     * @throws EmptyBunchDataException
     */
    public function last()
    {
        $this->countCheck();
        return end($this->data);
    }

    /**
     * @param $item
     * @return $this
     */
    public function append($item): self
    {
        $this->data[] = $item;

        return $this;
    }

    /**
     * @param $item
     * @return $this
     */
    public function prepend($item): self
    {
        $this->data[] = $item;

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     * @throws IncorrectIndexedBunchDataException
     */
    public function rewriteAll(array $data): Bunch
    {
        if (array_values($data) !== $data) {
            throw new IncorrectIndexedBunchDataException($data);
        }

        $this->data = $data;

        return $this;
    }
}

/**
 * Class AssocScope
 */
class AssocBunch extends Bunch
{
    /**
     * AssocInput constructor.
     * @param array $data
     * @throws IncorrectAssocBunchDataException
     */
    public function __construct(array $data = [])
    {
        if (!empty($data) && array_values($data) === $data) {
            throw new IncorrectAssocBunchDataException($data);
        }

        parent::__construct($data);
    }

    public function isset(string $key)
    {
        return isset($this->data[$key]);
    }

    /**
     * @param string $key
     * @throws BunchKeyDoesNotExistException
     */
    private function checkScopeKeyExists(string $key)
    {
        if (!array_key_exists($key, $this->data)) {
            throw new BunchKeyDoesNotExistException($key);
        }
    }

    /**
     * @param string $key
     * @throws BunchKeyAlreadyExistsException
     */
    private function checkScopeKeyDoesNotExist(string $key)
    {
        if (isset($this->data[$key])) {
            throw new BunchKeyAlreadyExistsException($key);
        }
    }


    /**
     * @param string $key
     * @param bool $mustBeSet
     * @param null $defaultValue
     * @return mixed
     * @throws BunchKeyDoesNotExistException
     */
    public function get(string $key, bool $mustBeSet = true, $defaultValue = null)
    {
        if ($mustBeSet) {
            $this->checkScopeKeyExists($key);
            return $this->data[$key];
        }


        return (isset($this->data[$key])) ? $this->data[$key] : $defaultValue;
    }

    /**
     * @param string $key
     * @param $value
     * @return $this
     * @throws BunchKeyAlreadyExistsException
     */
    public function set(string $key, $value): self
    {
        $this->checkScopeKeyDoesNotExist($key);
        $this->data[$key] = $value;

        return $this;
    }

    /**
     * @param string $key
     * @param $value
     * @return $this
     * @throws BunchKeyDoesNotExistException
     */
    public function reset(string $key, $value): self
    {

        $this->checkScopeKeyExists($key);
        $this->data[$key] = $value;

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     * @throws IncorrectAssocBunchDataException
     */
    public function rewriteAll(array $data): Bunch
    {
        if (array_values($data) === $data) {
            throw new IncorrectAssocBunchDataException($data);
        }

        $this->data = $data;

        return $this;
    }

    /**
     * @param array $data
     * @throws DataKeyExistsException
     */
    private function checkDataKeyExists(array $data)
    {
        foreach ($this->data as $key => $value) {
            if (array_key_exists($key, $data)) {
                throw new DataKeyExistsException($key);
            }
        }
    }

    public function mergeAppend(array $data): Bunch
    {
        /* @noinspection PhpUnhandledExceptionInspection */
        $this->checkDataKeyExists($data);

        return parent::mergeAppend($data);
    }

    public function mergePrepend(array $data): Bunch
    {
        /* @noinspection PhpUnhandledExceptionInspection */
        $this->checkDataKeyExists($data);

        return parent::mergePrepend($data);
    }
}
