<?php /** @noinspection PhpUnused */

class PhpFileTemplateInput extends RenderScriptUnitInput
{
    private $namespace;

    /**
     * @var IndexedBunch
     */
    private $useSection;

    public function __construct()
    {
        $this->useSection = new IndexedBunch();
    }

    public function setNamespace(string $namespace): self
    {
        $this->namespace = $namespace;

        return $this;
    }

    public function getNamespace()
    {
        return $this->namespace;
    }

    public function addUseSectionItem(string $useItem): self
    {
        $this->useSection->append($useItem);

        return $this;
    }

    public function getUseSection()
    {
        return $this->useSection;
    }

    public function getAllUseSectionItems()
    {
        return $this->useSection->all();
    }
}

class PhpClassTemplateInput extends RenderScriptUnitInput
{
    private $classTitle;

    private $classType;

    private $extends;

    public function setClassTitle(string $classTitle): self
    {
        $this->classTitle = $classTitle;

        return $this;
    }

    public function getClassTitle(): string
    {
        return $this->classTitle;
    }

    public function setClassType(string $classType = null): self
    {
        $this->classType = $classType;

        return $this;
    }

    public function getClassType(): ?string
    {
        return $this->classType;
    }

    public function setExtends(string $extends = null): self
    {
        $this->extends = $extends;

        return $this;
    }

    public function getExtends(): ?string
    {
        return $this->extends;
    }
}

class PhpClassParamTemplateInput extends RenderScriptUnitInput
{
    /**
     * @var string
     */
    private $title;

    private $access;

    /**
     * @var string|null
     */
    private $type;

    private $defaultValue;

    private $setterReturnType;


    /**
     * @param string|null $type
     * @return $this
     */
    public function setType(string $type = null): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $access
     * @return $this
     */
    public function setAccess(string $access = null): self
    {
        $this->access = (empty($access)) ? 'private' : $access;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccess(): string
    {
        return $this->access;
    }

    /**
     * @param string|null $defaultValue
     * @return $this
     */
    public function setDefaultValue(string $defaultValue = null): self
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDefaultValue(): ?string
    {
        return $this->defaultValue;
    }

    /**
     * @param string|null $setterReturnType
     * @return $this
     */
    public function setSetterReturnType(string $setterReturnType = null): self
    {
        $this->setterReturnType = (empty($access)) ? 'self' : $setterReturnType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSetterReturnType(): ?string
    {
        return $this->setterReturnType;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title): self
    {
        $this->title = $title;

        return $this;
    }
}