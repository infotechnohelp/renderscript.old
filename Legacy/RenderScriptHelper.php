<?php

class RenderScriptHelper
{
    static function prepareDefaultValueString($value)
    {
        if (empty($value)) {
            return null;
        }

        if($value === '_null'){
            return " = null";
        }

        return " = '$value'";
    }
}