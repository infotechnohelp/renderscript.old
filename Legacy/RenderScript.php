<?php /** @noinspection PhpUnused */

// EXCEPTIONS

class InputAlreadySetException extends Exception
{
    public function __construct()
    {
        parent::__construct("RenderScript unit's input is already set");
    }
}

class OutputDoesNotContainNeedleException extends Exception
{
    public function __construct(string $needle, string $output)
    {
        parent::__construct("Output does not contain needle '$needle'.  OUTPUT:\n$output\n");
    }
}


class OutputHasDuplicatedNeedlesException extends Exception
{
    public function __construct(string $needle, string $output)
    {
        parent::__construct("Output has duplicated needles '$needle'.  OUTPUT:\n$output\n");

    }
}

class OutputHasDuplicatedReservedNeedlesException extends Exception
{
    public function __construct(string $needle, string $output)
    {
        parent::__construct("Output has duplicated reserved needles '$needle'.  OUTPUT:\n$output\n");

    }
}


// CLASSES

class RenderScriptUnit
{
// Outputs
    private $output;

// Unit tree
    /**
     * @var RenderScriptUnit
     */
    private $rootUnit;

// Inputs

    /**
     * @var AssocBunch
     *
     * @todo Separate Input class required (extends AssocBunch)
     */
    private $input;

    /**
     * @var AssocBunch
     */
    private $config;

    public function setConfig(AssocBunch $config): self
    {
        $this->config = $config;

        return $this;
    }

    public function getConfig()
    {
        return $this->config;
    }


    private $reservedNeedles;


// Markers

    private $needle = "#"; // @todo Config required

    private $reservedNeedleOpening = '{{{'; // @todo Config required
    private $reservedNeedleClosing = '}}}'; // @todo Config required


    public function __construct(RenderScriptUnit $rootUnit = null)
    {
        $this->rootUnit = $rootUnit;

        $this->output = "{$this->getNeedle()}";

        $this->reservedNeedles = new AssocBunch();
    }

    public function getRoot()
    {
        return $this->rootUnit;
    }

    public function getNeedle()
    {
        return $this->needle;
    }

    public function getReservedNeedleOpening()
    {
        return $this->reservedNeedleOpening;
    }

    public function getReservedNeedleClosing()
    {
        return $this->reservedNeedleClosing;
    }

    /**
     * @param string $key
     * @param $value
     * @return $this
     * @throws BunchKeyAlreadyExistsException
     */
    public function setReservedNeedle(string $key, $value): self
    {
        $this->reservedNeedles->set($key, $value);

        return $this;
    }

    /**
     * @param string $key
     * @return mixed
     * @throws BunchKeyDoesNotExistException
     */
    public function getReservedNeedle(string $key)
    {
        return $this->reservedNeedles->get($key);
    }

    /**
     * @return array
     * @throws EmptyBunchDataException
     */
    public function getReservedNeedles()
    {
        return $this->reservedNeedles->all(true);
    }


    /**
     * @param AssocBunch $input
     * @return $this
     * @throws InputAlreadySetException
     */
    public function setInput(AssocBunch $input): self
    {
        if ($this->input !== null) {
            throw new InputAlreadySetException();
        }

        $this->input = $input;
        return $this;
    }

    public function appendInput(array $data): self
    {
        $this->input->mergeAppend($data);

        return $this;
    }

    public function prependInput(array $data): self
    {
        $this->input->mergePrepend($data);

        return $this;
    }

    public function resetInputValue(string $key, $value): self
    {
        /* @noinspection PhpUnhandledExceptionInspection */
        $this->input->reset($key, $value);

        return $this;
    }

    public function getInput()
    {
        return $this->input;
    }


// @todo Share output rendering method required

    /**
     * @param string $classTitle
     * @param null $processingParams
     * @return $this
     * @throws OutputDoesNotContainNeedleException
     * @throws OutputHasDuplicatedNeedlesException
     */
    public function replaceNeedle(string $classTitle, $processingParams = null): self
    {
        $this->checkNeedleIsPresent();
        $this->checkNoNeedleDuplicates();

        /** @var RenderScriptUnit $object */
        $object = new $classTitle($this);

        $result = $object->render($processingParams)->getOutput();

        $this->output = str_replace($this->getNeedle(), $result, $this->output);

        return $this;
    }

    /**
     * @throws OutputDoesNotContainNeedleException
     */
    private function checkNeedleIsPresent()
    {
        if (strpos($this->output, $this->getNeedle()) === false) {
            throw new OutputDoesNotContainNeedleException($this->getNeedle(), $this->output);
        }
    }

    /**
     * @throws OutputHasDuplicatedNeedlesException
     */
    private function checkNoNeedleDuplicates()
    {
        $pos = 0;
        $i = 0;

        while (($pos = strpos(($this->output), $this->getNeedle(), $pos + 1)) !== false) {
            if ($i > 1) {
                throw new OutputHasDuplicatedNeedlesException($this->getNeedle(), $this->output);
            }

            $i++;
        }
    }

    /**
     * @param string $reservedNeedleKey
     * @throws OutputHasDuplicatedReservedNeedlesException
     */
    private function checkNoReservedNeedleDuplicates(string $reservedNeedleKey)
    {
        $needle = "{$this->getReservedNeedleOpening()}$reservedNeedleKey{$this->getReservedNeedleClosing()}";

        $pos = 0;
        $i = 0;

        while (($pos = strpos(($this->output), $needle, $pos + 1)) !== false) {
            if ($i > 1) {
                throw new OutputHasDuplicatedReservedNeedlesException($this->getNeedle(), $this->output);
            }

            $i++;
        }
    }

    /**
     * @param string $classTitle
     * @param null $processingParams
     * @return $this
     * @throws OutputDoesNotContainNeedleException
     * @throws OutputHasDuplicatedNeedlesException
     */
    public function prependNeedle(string $classTitle, $processingParams = null): self
    {
        $this->checkNeedleIsPresent();
        $this->checkNoNeedleDuplicates();

        /** @var RenderScriptUnit $object */
        $object = new $classTitle($this);

        $result = $object->render($processingParams)->getOutput();

        $needlePosition = strpos($this->output, $this->getNeedle());

        $this->output = substr($this->output, 0, $needlePosition) . $result . substr($this->output, $needlePosition);

        return $this;
    }

    /**
     * @param bool $multiple
     * @return $this
     * @throws EmptyBunchDataException
     * @throws OutputHasDuplicatedReservedNeedlesException
     */
    public function renderReservedNeedles(bool $multiple = true): self
    {

        foreach ($this->getReservedNeedles() as $reservedNeedleKey => $reservedNeedleData) {

            $output = $reservedNeedleData;

            if ($reservedNeedleData instanceof IndexedBunch) {
                $output = implode($reservedNeedleData->all());
            }

            if (!$multiple) {
                /* @noinspection PhpUnhandledExceptionInspection */
                $this->checkNoReservedNeedleDuplicates($reservedNeedleKey);
            }

            $this->output = str_replace(
                "{$this->getReservedNeedleOpening()}$reservedNeedleKey{$this->getReservedNeedleClosing()}",
                $output,
                $this->output
            );
        }

        return $this;
    }

    /**
     * @return $this
     * @throws OutputDoesNotContainNeedleException
     * @throws OutputHasDuplicatedNeedlesException
     */
    public function removeNeedle(): self
    {
        $this->checkNeedleIsPresent();
        $this->checkNoNeedleDuplicates();

        $this->output = str_replace(
            $this->getNeedle(),
            "",
            $this->output
        );


        return $this;
    }

    public function getOutput()
    {
        return $this->output;
    }

    /** @noinspection PhpPossiblePolymorphicInvocationInspection */
    public function render($processingParams = null): self
    {
        $this->output = $this->process($processingParams);

        return $this;
    }
}


class RenderScriptUnitInput
{

}


class RenderScriptUnitConfig
{

}

class RenderScriptTemplate extends RenderScriptUnit
{
    protected function prepareConfig()
    {
        $classTitle = get_class($this);

        $configClassTitle = "{$classTitle}Config";

        return $this->getRoot()->getConfig()->get($classTitle, false, new $configClassTitle);
    }

    protected function prepareInput()
    {
        if (empty($this->getRoot()->getInput())) {
            return null;
        }

        return $this->getRoot()->getInput()->get(get_class($this), false, null);
    }
}

class RenderScriptTemplateBunch extends RenderScriptTemplate
{

}